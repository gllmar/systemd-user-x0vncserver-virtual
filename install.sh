echo "installing dependencies via pacman"
echo "copying service to user service folder"
mkdir -p ~/.config/systemd/user/
cp x0vncserver-virtual1.service ~/.config/systemd/user/x0vncserver-virtual1.service
cp x0vncserver-virtual2.service ~/.config/systemd/user/x0vncserver-virtual2.service
echo "reload user daemon activation will be started when xorg is configured"
systemctl --user daemon-reload
