xrandr --addmode VIRTUAL1 1920x1080
xrandr \
--output VIRTUAL1 \
--mode 1920x1080 \
--pos 0x1080 \
--rotate normal

xrandr | grep VIRTUAL1

sleep 1
xrandr --addmode VIRTUAL2 1920x1080
sleep 1
xrandr \
--output VIRTUAL2 \
--mode 1920x1080 \
--pos 0x2160 \
--rotate normal

xrandr | grep VIRTUAL2
